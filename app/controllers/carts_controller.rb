class CartsController < ApplicationController
  def index
    @order_items = current_order.order_items
  end
  
  def show
    @cart = Cart.find(params[:id])
  end
	
  def new
    @cart = Cart.new
  end
  
  def create
    @cart = Cart.new(cart_params)
    if @cart.save
      redirect_to carts_path
    else
      render 'new'
    end
  end
  
  def update
    @cart = Cart.new(cart_params)
    if @cart.update_attributes(cart_params)
      redirect_to carts_path
    else
      render 'edit'
    end
  end

  private
  def cart_params
    params.require(:cart).permit(:order_item_id, :order_id, :order_message, :delivery, :payment_method)
  end
  
end
