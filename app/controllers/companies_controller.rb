class CompaniesController < ApplicationController
  before_action :set_company, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:index, :show]

  # GET /companies
  # GET /companies.json
  def index
    @companies = Company.all
    
   #scopes
   @companies = @companies.food_type(params[:food_type]) if params[:food_type].present?
   @companies = @companies.delivery(true) if params[:delivery].present?
   
  end

  # GET /companies/1
  # GET /companies/1.json
  def show
    @Companies = Company.friendly.find(params[:id])
    @Products = Product.where(company_id: @company.id)
    @order_item = current_order.order_items.new
  end

  # GET /companies/new
  def new
    @company = Company.new
    @cities = City.all
    @districts = District.all
  end

  # GET /companies/1/edit
  def edit
    @cities = City.all
    @districts = District.all
  end

  # POST /companies
  # POST /companies.json
  def create
    @company = Company.new(company_params)

    respond_to do |format|
      if @company.save
        format.html { redirect_to @company, notice: 'Firma başarıyla oluşturuldu.' }
        format.json { render :show, status: :created, location: @company }
      else
        format.html { render :new }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /companies/1
  # PATCH/PUT /companies/1.json
  def update
    respond_to do |format|
      if @company.update(company_params)
        format.html { redirect_to @company, notice: 'Firma başarıyla güncellendi.' }
        format.json { render :show, status: :ok, location: @company }
      else
        format.html { render :edit }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /companies/1
  # DELETE /companies/1.json
  def destroy
    @company.destroy
    respond_to do |format|
      format.html { redirect_to companies_url, notice: 'Firma başarıyla silindi.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company
      @company = Company.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_params
      params.require(:company).permit(:name, :manager, :address, :company_phone, :manager_phone, :tax_number, :food_type, :email, :rate, :delivery, :city_id, :district_id, :url_path)
    end
end
