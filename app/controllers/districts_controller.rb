class DistrictsController < ApplicationController
  
  def index
  end
  
 def show
   @cities = City.all
   @city = City.friendly.find(params[:city_id])
   @districts = District.all
   @district = District.friendly.find(params[:id])
   @companies = @district.companies
   
   #scopes
   @companies = @companies.food_type(params[:food_type]) if params[:food_type].present?
   @companies = @city.companies.delivery(true) if params[:delivery].present?
 end
 
 private
    # Use callbacks to share common setup or constraints between actions.
    def find_district
      @district = District.friendly.find(params[:id])
    end
 
    def district_params
      params.require(:district).permit(:name, city_id, :slugged)
    end

  def new
  end
  
  def create
  end
  
  def edit
  end
  
  def update
  end
  
  def destroy
  end

end
