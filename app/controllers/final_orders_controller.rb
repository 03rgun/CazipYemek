class FinalOrdersController < ApplicationController
  before_action :set_final_order, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:new, :show, :create]

  # GET /final_orders
  # GET /final_orders.json
  def index
    @final_orders = FinalOrder.all
    @orders = Order.all
    @order_items = OrderItem.all
    #@Products = Product.where(company_id: @company.id)
  end

  # GET /final_orders/1
  # GET /final_orders/1.json
  def show
    @final_orders = FinalOrder.find(params[:id])
    @order_items = OrderItem.where(:id => @final_orders.order_item_id)
  end

  # GET /final_orders/new
  def new
    @final_order = FinalOrder.new
    @order_items = current_order.order_items
  end

  # GET /final_orders/1/edit
  def edit
  end

  # POST /final_orders
  # POST /final_orders.json
  def create
    @final_order = FinalOrder.new(final_order_params)

    respond_to do |format|
      if @final_order.save
        format.html { redirect_to @final_order, notice: 'Siparişiniz alındı. İşletmenin profilinde yazan indirimli saatlerde ürününüzü teslim alabilirsiniz.' }
        format.json { render :show, status: :created, location: @final_order }
      else
        format.html { render :new }
        format.json { render json: @final_order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /final_orders/1
  # PATCH/PUT /final_orders/1.json
  def update
    respond_to do |format|
      if @final_order.update(final_order_params)
        format.html { redirect_to @final_order, notice: 'Final order was successfully updated.' }
        format.json { render :show, status: :ok, location: @final_order }
      else
        format.html { render :edit }
        format.json { render json: @final_order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /final_orders/1
  # DELETE /final_orders/1.json
  def destroy
    @final_order.destroy
    respond_to do |format|
      format.html { redirect_to final_orders_url, notice: 'Final order was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_final_order
      @final_order = FinalOrder.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def final_order_params
      params.require(:final_order).permit(:order_item_id, :order_id, :order_message, :delivery, :payment_method, :name, :address, :phone)
    end
end
