class HomeController < ApplicationController
  
  def index
    @cities = City.all
    @companies = Company.all
  end
  
 def show
   @city = City.friendly.find(params[:id])
   @companies = @city.companies
   @districts = District.all
   
   #scopes
   @companies = @companies.food_type(params[:food_type]) if params[:food_type].present?
   @companies = @city.companies.delivery(true) if params[:delivery].present?
   
 end
 
 private
    # Use callbacks to share common setup or constraints between actions.
    def find_city
      @city = City.friendly.find(params[:id])
    end
 
    def city_params
      params.require(:city).permit(:name, :included, :slug)
    end

  def new
  end
  
  def create
  end
  
  def edit
  end
  
  def update
  end
  
  def destroy
  end

end
