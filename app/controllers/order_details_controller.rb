class OrderDetailsController < ApplicationController
  def index
    @carts = Cart.all
  end

  def show
    @order_items = current_order.order_items
  end

  def update
  end
  
  def edit
  end
end
