class City < ActiveRecord::Base
    extend FriendlyId
    friendly_id :name, :use => :slugged
    
    has_many :companies
    has_many :districts
end
