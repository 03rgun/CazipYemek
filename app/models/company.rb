class Company < ActiveRecord::Base
    extend FriendlyId
    friendly_id :url_path, :use => :slugged
    
    belongs_to :city
    belongs_to :district
    has_many :product
    
    scope :food_type, -> (food_type) { where food_type: food_type }
    scope :delivery, -> (delivery) { where delivery: delivery } 
end
