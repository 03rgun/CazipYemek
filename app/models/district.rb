class District < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, :use => :slugged
  
  belongs_to :city
  has_many :companies
end
