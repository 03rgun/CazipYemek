json.extract! company, :id, :name, :manager, :city, :district, :address, :company_phone, :manager_phone, :tax_number, :food_type, :email, :rate, :delivery, :created_at, :updated_at
json.url company_url(company, format: :json)
