json.extract! final_order, :id, :order_item_id, :order_id, :order_message, :delivery, :payment_method, :created_at, :updated_at
json.url final_order_url(final_order, format: :json)
