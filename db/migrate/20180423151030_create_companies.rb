class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.string :manager
      t.string :city
      t.string :district
      t.string :address
      t.string :company_phone
      t.string :manager_phone
      t.string :tax_number
      t.string :food_type
      t.string :email
      t.decimal :rate
      t.boolean :delivery

      t.timestamps null: false
    end
  end
end
