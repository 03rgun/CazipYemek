class AddIncludedToCities < ActiveRecord::Migration
  def change
    add_column :cities, :included, :boolean
  end
end
