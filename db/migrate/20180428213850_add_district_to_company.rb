class AddDistrictToCompany < ActiveRecord::Migration
  def change
    add_reference :companies, :district, index: true, foreign_key: true
  end
end
