class AddSlugToDistricts < ActiveRecord::Migration
  def change
    add_column :districts, :slug, :string
    add_index :districts, :slug, unique: true
  end
end
