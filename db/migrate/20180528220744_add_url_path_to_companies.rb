class AddUrlPathToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :url_path, :string
    add_index :companies, :url_path, unique: true
  end
end
