class AddMenuIdToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :menu_id, :integer
  end
end
