class AddCreditCardToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :credit_card, :boolean
  end
end
