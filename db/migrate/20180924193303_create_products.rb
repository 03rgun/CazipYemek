class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.integer :company_id
      t.string :title
      t.float :normal_price
      t.float :discounted_price
      t.integer :remaining_stock
      t.string :discounted_time

      t.timestamps null: false
    end
  end
end
