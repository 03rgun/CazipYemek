class CreateCarts < ActiveRecord::Migration
  def change
    create_table :carts do |t|
      t.string :order_item_id
      t.string :order_id
      t.string :order_message
      t.boolean :delivery
      t.string :payment_method

      t.timestamps null: false
    end
  end
end
