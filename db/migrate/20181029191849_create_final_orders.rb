class CreateFinalOrders < ActiveRecord::Migration
  def change
    create_table :final_orders do |t|
      t.string :order_item_id
      t.string :order_id
      t.string :order_message
      t.string :delivery
      t.string :payment_method

      t.timestamps null: false
    end
  end
end
