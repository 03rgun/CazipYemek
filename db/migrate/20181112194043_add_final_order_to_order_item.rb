class AddFinalOrderToOrderItem < ActiveRecord::Migration
  def change
    add_reference :order_items, :FinalOrder, index: true, foreign_key: true
  end
end
