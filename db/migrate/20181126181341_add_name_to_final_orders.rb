class AddNameToFinalOrders < ActiveRecord::Migration
  def change
    add_column :final_orders, :name, :string
  end
end
