class AddAddressToFinalOrders < ActiveRecord::Migration
  def change
    add_column :final_orders, :address, :string
  end
end
