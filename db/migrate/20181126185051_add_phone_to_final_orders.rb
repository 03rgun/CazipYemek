class AddPhoneToFinalOrders < ActiveRecord::Migration
  def change
    add_column :final_orders, :phone, :string
  end
end
