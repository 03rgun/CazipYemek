# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181206202625) do

  create_table "carts", force: :cascade do |t|
    t.string   "order_item_id"
    t.string   "order_id"
    t.string   "order_message"
    t.boolean  "delivery"
    t.string   "payment_method"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "cities", force: :cascade do |t|
    t.string  "name"
    t.boolean "included"
    t.string  "slug"
  end

  add_index "cities", ["slug"], name: "index_cities_on_slug", unique: true

  create_table "companies", force: :cascade do |t|
    t.string   "name"
    t.string   "manager"
    t.string   "city"
    t.string   "district"
    t.string   "address"
    t.string   "company_phone"
    t.string   "manager_phone"
    t.string   "tax_number"
    t.string   "food_type"
    t.string   "email"
    t.decimal  "rate"
    t.boolean  "delivery"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "city_id"
    t.integer  "district_id"
    t.string   "slug"
    t.string   "url_path"
    t.integer  "menu_id"
    t.boolean  "credit_card"
  end

  add_index "companies", ["city_id"], name: "index_companies_on_city_id"
  add_index "companies", ["district_id"], name: "index_companies_on_district_id"
  add_index "companies", ["slug"], name: "index_companies_on_slug", unique: true
  add_index "companies", ["url_path"], name: "index_companies_on_url_path", unique: true

  create_table "districts", force: :cascade do |t|
    t.string  "name"
    t.integer "city_id"
    t.string  "slug"
  end

  add_index "districts", ["city_id"], name: "index_districts_on_city_id"
  add_index "districts", ["slug"], name: "index_districts_on_slug", unique: true

  create_table "final_orders", force: :cascade do |t|
    t.string   "order_item_id"
    t.string   "order_id"
    t.string   "order_message"
    t.string   "delivery"
    t.string   "payment_method"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "name"
    t.string   "address"
    t.string   "phone"
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id"
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type"

  create_table "menus", force: :cascade do |t|
    t.string   "company_id"
    t.string   "name"
    t.decimal  "price"
    t.decimal  "discounted_price"
    t.integer  "stock"
    t.string   "discounted_time"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "order_items", force: :cascade do |t|
    t.integer  "product_id"
    t.integer  "order_id"
    t.float    "unit_price"
    t.integer  "quantity"
    t.integer  "total_price"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "FinalOrder_id"
  end

  add_index "order_items", ["FinalOrder_id"], name: "index_order_items_on_FinalOrder_id"

  create_table "orders", force: :cascade do |t|
    t.float    "subtotal"
    t.float    "total"
    t.float    "tax"
    t.float    "delivery_fee"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "products", force: :cascade do |t|
    t.integer  "company_id"
    t.string   "title"
    t.float    "normal_price"
    t.float    "discounted_price"
    t.integer  "remaining_stock"
    t.string   "discounted_time"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "receipts", force: :cascade do |t|
    t.string   "order_item_id"
    t.string   "order_id"
    t.string   "order_message"
    t.boolean  "delivery"
    t.string   "payment_method"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
