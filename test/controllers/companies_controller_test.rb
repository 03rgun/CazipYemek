require 'test_helper'

class CompaniesControllerTest < ActionController::TestCase
  setup do
    @company = companies(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:companies)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create company" do
    assert_difference('Company.count') do
      post :create, company: { address: @company.address, city: @company.city, company_phone: @company.company_phone, delivery: @company.delivery, district: @company.district, email: @company.email, food_type: @company.food_type, manager: @company.manager, manager_phone: @company.manager_phone, name: @company.name, rate: @company.rate, tax_number: @company.tax_number }
    end

    assert_redirected_to company_path(assigns(:company))
  end

  test "should show company" do
    get :show, id: @company
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @company
    assert_response :success
  end

  test "should update company" do
    patch :update, id: @company, company: { address: @company.address, city: @company.city, company_phone: @company.company_phone, delivery: @company.delivery, district: @company.district, email: @company.email, food_type: @company.food_type, manager: @company.manager, manager_phone: @company.manager_phone, name: @company.name, rate: @company.rate, tax_number: @company.tax_number }
    assert_redirected_to company_path(assigns(:company))
  end

  test "should destroy company" do
    assert_difference('Company.count', -1) do
      delete :destroy, id: @company
    end

    assert_redirected_to companies_path
  end
end
