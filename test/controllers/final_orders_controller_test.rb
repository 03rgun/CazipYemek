require 'test_helper'

class FinalOrdersControllerTest < ActionController::TestCase
  setup do
    @final_order = final_orders(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:final_orders)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create final_order" do
    assert_difference('FinalOrder.count') do
      post :create, final_order: { delivery: @final_order.delivery, order_id: @final_order.order_id, order_item_id: @final_order.order_item_id, order_message: @final_order.order_message, payment_method: @final_order.payment_method }
    end

    assert_redirected_to final_order_path(assigns(:final_order))
  end

  test "should show final_order" do
    get :show, id: @final_order
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @final_order
    assert_response :success
  end

  test "should update final_order" do
    patch :update, id: @final_order, final_order: { delivery: @final_order.delivery, order_id: @final_order.order_id, order_item_id: @final_order.order_item_id, order_message: @final_order.order_message, payment_method: @final_order.payment_method }
    assert_redirected_to final_order_path(assigns(:final_order))
  end

  test "should destroy final_order" do
    assert_difference('FinalOrder.count', -1) do
      delete :destroy, id: @final_order
    end

    assert_redirected_to final_orders_path
  end
end
